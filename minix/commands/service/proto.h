void fatal(char *fmt, ...);
void warning(char *fmt, ...);
const char *parse_config(char *progname, int custom, char *configname,
	struct rs_config *config);
const char *parse_dcfi_config(char *progname, char *dcfi_configname,
  struct rs_config *config);
