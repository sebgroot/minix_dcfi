#ifndef _DCFI_H
#define _DCFI_H

#include <minix/privdcfi.h>
#include "kernel/proc.h"

#define DCFI_OK 1
#define DCFI_EALLOC -1
#define DCFI_ENOTENABLED -2
#define DCFI_ENOTALLOWED -3
#define DCFI_ENOTIMPLEMENTED -4
#define DCFI_ENODEEXISTS -5
#define DCFI_ENOSUCHNODE -6
#define DCFI_EOUTGOINGMAX -7
#define DCFI_ERROR -8

#define DCFI_POOLSIZE 1024
#define DCFI_OUTGOING_MAX 16

/* NR_PROCS + kernel + any processes as source */
#define DCFI_NR_SRCS (NR_PROCS+2)

struct dcfi_src_node {
  endpoint_t src;
  struct dcfi_dst_node *dst_node_list;
  struct dcfi_src_node *next;
};

struct dcfi_dst_node {
  endpoint_t dst;
  struct dcfi_msg_node *msg_node_list;
  struct dcfi_dst_node *next;
};

struct dcfi_msg_node {
  int m_type;
  struct dcfi_dst_node *dst_node;
  struct dcfi_msg_node *children[DCFI_OUTGOING_MAX];
  int n_children;
  struct dcfi_msg_node *next;
};

void dcfi_patch_ep(endpoint_t from, endpoint_t to);
int dcfi_cleanup(endpoint_t ep);
int dcfi_add_msg_node(endpoint_t src, endpoint_t dst, int m_type);
int dcfi_add_msg_child(endpoint_t p_src, endpoint_t p_dst, int p_m_type, endpoint_t c_src, endpoint_t c_dst, int c_m_type);
int dcfi_set_default(struct proc *rp, endpoint_t src, endpoint_t dst, int m_type);
void dcfi_recv(endpoint_t src, struct proc *dst, int m_type);
int dcfi_check(struct proc *caller, endpoint_t dst, int m_type);

#endif
