/* This file contains the implementation of the Distributed Control Flow
 * Integrity of the MINIX kernel.
 * Three functions are used to interface with this module:
 *
 *    dcfi_add_rule:    Add an allowed path to the caller's proc struct.
 *    dcfi_del_rule:    Delete an allowed path from the caller's proc struct.
 *    dcfi_check:       Check if caller is allowed to send a message to
 *                      dst using a given m_type.
 * Changes:
 *    Jul 13, 2015      initial implementation (Sebastiaan Groot)
 *
 * This module is called by:
 *    proc.c:(try_deliver_senda && do_sync_ipc && mini_receive && try_one)
 *    system.c:(kernel_call && kernel_call_finish)
 */

#include "kernel/kernel.h"
#include "proc.h"
#include "priv.h"
#include "dcfi.h"

/* Lookup msg_node */
static struct dcfi_src_node *src_node_list = NULL;

/* Allocation pools for DCFI */
static struct dcfi_msg_node dcfi_msg_node_pool[DCFI_POOLSIZE];
static bitchunk_t dcfi_msg_node_map[BITMAP_CHUNKS(DCFI_POOLSIZE)] = { 0 };

static struct dcfi_dst_node dcfi_dst_node_pool[DCFI_POOLSIZE];
static bitchunk_t dcfi_dst_node_map[BITMAP_CHUNKS(DCFI_POOLSIZE)] = { 0 };

static struct dcfi_src_node dcfi_src_node_pool[DCFI_NR_SRCS]; /* NR_PROCS + room for two special identifiers */
static bitchunk_t dcfi_src_node_map[BITMAP_CHUNKS(DCFI_NR_SRCS)] = { 0 };

/* Generic free & alloc functions. Internal use only. */
static void _dcfi_free(void *ptr, void *pool, bitchunk_t *map, int pool_max, int stride)
{
  int i = 0;
  while (pool + (i * stride) != ptr && i < pool_max) i++;

  if (pool + (i * stride) != ptr)
    return;

  UNSET_BIT(map, i);
}

static void *_dcfi_alloc(void *pool, bitchunk_t *map, int pool_max, int stride)
{
  int i;
  for (i = 0; i < pool_max; i++)
  {
    if (GET_BIT(map, i) == 0)
    {
      SET_BIT(map, i);
      return (pool + (i * stride));
    }
  }

  return NULL;
}

/* Free & alloc wrappers for specific pools */
static void dcfi_msg_node_free(struct dcfi_msg_node *ptr)
{
  _dcfi_free(
    ptr,
    dcfi_msg_node_pool,
    dcfi_msg_node_map,
    DCFI_POOLSIZE,
    sizeof(struct dcfi_msg_node)
  );
}

static struct dcfi_msg_node *dcfi_msg_node_alloc()
{
  return (struct dcfi_msg_node *) _dcfi_alloc(
    dcfi_msg_node_pool,
    dcfi_msg_node_map,
    DCFI_POOLSIZE,
    sizeof(struct dcfi_msg_node)
  );
}

static void dcfi_dst_node_free(struct dcfi_dst_node *ptr)
{
  _dcfi_free(
    ptr,
    dcfi_dst_node_pool,
    dcfi_dst_node_map,
    DCFI_POOLSIZE,
    sizeof(struct dcfi_dst_node)
  );
}

static struct dcfi_dst_node *dcfi_dst_node_alloc()
{
  return (struct dcfi_dst_node *) _dcfi_alloc(
    dcfi_dst_node_pool,
    dcfi_dst_node_map,
    DCFI_POOLSIZE,
    sizeof(struct dcfi_dst_node)
  );
}

static void dcfi_src_node_free(struct dcfi_src_node *ptr)
{
  _dcfi_free(
    ptr,
    dcfi_src_node_pool,
    dcfi_src_node_map,
    DCFI_NR_SRCS,
    sizeof(struct dcfi_src_node)
  );
}

static struct dcfi_src_node *dcfi_src_node_alloc()
{
  return (struct dcfi_src_node *) _dcfi_alloc(
    dcfi_src_node_pool,
    dcfi_src_node_map,
    DCFI_NR_SRCS,
    sizeof(struct dcfi_src_node)
  );
}

static struct dcfi_msg_node *dcfi_get_msg_node(endpoint_t src, endpoint_t dst, int m_type)
{
  struct dcfi_src_node *src_node;
  struct dcfi_dst_node *dst_node = NULL;
  struct dcfi_msg_node *msg_node = NULL;

  for (src_node = src_node_list; src_node; src_node = src_node->next)
  {
    if (src_node->src == src)
    {
      dst_node = src_node->dst_node_list;
      break;
    }
  }

  for (; dst_node; dst_node = dst_node->next)
  {
    if (dst_node->dst == dst)
    {
      msg_node = dst_node->msg_node_list;
      break;
    }
  }

  for (; msg_node; msg_node = msg_node->next)
    if (msg_node->m_type == m_type)
      return msg_node;

  return NULL;
}

/* deletes both the msg_node and all of its appearances as childs */
static int dcfi_del_msg_node(endpoint_t src, endpoint_t dst, int m_type)
{
  struct dcfi_src_node *src_node = src_node_list, *src_node_prev = NULL;
  struct dcfi_dst_node *dst_node = NULL, *dst_node_prev = NULL;
  struct dcfi_msg_node *msg_node = NULL, *del_node = NULL, *del_node_prev = NULL;
  int i, j;

  /* Find the node to be deleted and remember previous nodes */
  for (src_node = src_node_list; src_node; src_node = src_node->next)
  {
    if (src_node->src == src)
    {
      dst_node = src_node->dst_node_list;
      break;
    }
    src_node_prev = src_node;
  }
  for (; dst_node; dst_node = dst_node->next)
  {
    if (dst_node->dst == dst)
    {
      del_node = dst_node->msg_node_list;
      break;
    }
    dst_node_prev = dst_node;
  }
  for (; del_node; del_node = del_node->next)
  {
    if (del_node->m_type == m_type)
    {
      break;
    }
    del_node_prev = del_node;
  }

  /* src-m_type->dst does not exist */
  if (del_node == NULL)
    return DCFI_ENOSUCHNODE;

  /* remove the del_node from its linked list */
  if (del_node_prev != NULL)
  {
    del_node_prev->next = del_node->next;
  }
  else if (del_node->next != NULL)
  {
    dst_node->msg_node_list = del_node->next;
  }
  else
  {
    /* del_node was the last in its list, delete dst_node */
    if (dst_node_prev != NULL)
    {
      dst_node_prev->next = dst_node->next;
    }
    else if (dst_node->next != NULL)
    {
      src_node->dst_node_list = dst_node->next;
    }
    else
    {
      /* dst_node was also the last in its list, delete src_node */
      if (src_node_prev != NULL)
      {
        src_node_prev->next = src_node->next;
      }
      else if (src_node->next != NULL)
      {
        src_node_list = src_node->next;
      }
      else
      {
        src_node_list = NULL;
      }
      dcfi_src_node_free(src_node);
    }
    dcfi_dst_node_free(dst_node);
  }

  /* first delete all appearances of msg_node in children */
  for (src_node = src_node_list; src_node; src_node = src_node->next)
  {
    for (dst_node = src_node->dst_node_list; dst_node; dst_node = dst_node->next)
    {
      for (msg_node = dst_node->msg_node_list; msg_node; msg_node = msg_node->next)
      {
        if (msg_node->n_children > DCFI_OUTGOING_MAX)
        {
          return DCFI_ERROR;
        }
        for (i = 0; i < msg_node->n_children; i++)
        {
          if (msg_node->children[i] == del_node)
          {
            for (j = i+1; j < msg_node->n_children; j++)
            {
              msg_node->children[j-1] = msg_node->children[j];
            }
            msg_node->n_children--;
          }
        }
      } /* for (msg_node..) */
    } /* for (dst_node..) */
  } /* for (src_node..) */

  /* we are finally able to free the msg_node to be deleted */
  dcfi_msg_node_free(del_node);

  return DCFI_OK;
}

static int dcfi_cleanup_get_msg(endpoint_t ep, endpoint_t *src, endpoint_t *dst, int *m_type)
{
  struct dcfi_src_node *src_node;
  struct dcfi_dst_node *dst_node = NULL;
  struct dcfi_msg_node *msg_node = NULL;

  for (src_node = src_node_list; src_node; src_node = src_node->next)
  {
    for (dst_node = src_node->dst_node_list; dst_node; dst_node = dst_node->next)
    {
      for (msg_node = dst_node->msg_node_list; msg_node; msg_node = msg_node->next)
      {
        if (src_node->src == ep || dst_node->dst == ep)
        {
          *src = src_node->src;
          *dst = dst_node->dst;
          *m_type = msg_node->m_type;
          return DCFI_OK;
        }
      }
    }
  }
  return DCFI_ENOSUCHNODE;
}
/* public functions */
#ifdef USE_DCFI
void dcfi_patch_ep(endpoint_t from, endpoint_t to)
{
  struct dcfi_src_node *src_node;
  struct dcfi_dst_node *dst_node;
  int count = 0;

  for (src_node = src_node_list; src_node; src_node = src_node->next)
  {
    if (src_node->src == from)
    {
      src_node->src = to;
      count++;
    }

    for (dst_node = src_node->dst_node_list; dst_node; dst_node = dst_node->next)
    {
      if (dst_node->dst == from)
      {
        dst_node->dst = to;
        count++;
      }
    }
  }
}

int dcfi_cleanup(endpoint_t ep)
{
  endpoint_t src, dst;
  int m_type;

  while (dcfi_cleanup_get_msg(ep, &src, &dst, &m_type) == DCFI_OK)
    if (dcfi_del_msg_node(src, dst, m_type) != DCFI_OK)
      return DCFI_ERROR;

  return DCFI_OK;
}

int dcfi_add_msg_node(endpoint_t src, endpoint_t dst, int m_type)
{
  struct dcfi_src_node *src_node = src_node_list;
  struct dcfi_dst_node *dst_node = NULL;
  struct dcfi_msg_node *msg_node = NULL;

  if (dcfi_get_msg_node(src, dst, m_type) != NULL)
    return DCFI_OK; /* node already exists */
  
  /* find / create the correct src_node */
  if (src_node == NULL)
  {
    src_node_list = dcfi_src_node_alloc();
    if (src_node_list == NULL)
      return DCFI_EALLOC;
    src_node_list->src = src;
    src_node_list->dst_node_list = NULL;
    src_node_list->next = NULL;
    src_node = src_node_list;
  }
  for (; src_node; src_node = src_node->next)
  {
    /* found correct src node */
    if (src_node->src == src)
    {
      dst_node = src_node->dst_node_list;
      break;
    }

    /* correct src node not yet in list */
    if (src_node->next == NULL)
    {
      src_node->next = dcfi_src_node_alloc();
      if (src_node->next == NULL)
        return DCFI_EALLOC;
      src_node = src_node->next;
      src_node->src = src;
      src_node->dst_node_list = NULL;
      src_node->next = NULL;
      break;
    }
  }
  
  /* find / create correct src_node->dst_node */
  if (dst_node == NULL)
  {
    src_node->dst_node_list = dcfi_dst_node_alloc();
    if (src_node->dst_node_list == NULL)
      return DCFI_EALLOC;

    dst_node = src_node->dst_node_list;
    dst_node->dst = dst;
    dst_node->msg_node_list = NULL;
    dst_node->next = NULL;
  }
  for (; dst_node; dst_node = dst_node->next)
  {
    /* found correct dst node */
    if (dst_node->dst == dst)
    {
      msg_node = dst_node->msg_node_list;
      break;
    }

    /* correct dst node not yet in list */
    if (dst_node->next == NULL)
    {
      dst_node->next = dcfi_dst_node_alloc();
      if (dst_node->next == NULL)
        return DCFI_EALLOC;
      dst_node = dst_node->next;
      dst_node->dst = dst;
      dst_node->msg_node_list = NULL;
      dst_node->next = NULL;
      break;
    }
  }
  
  /* find / create correct msg_node */
  if (msg_node == NULL)
  {
    dst_node->msg_node_list = dcfi_msg_node_alloc();
    if (dst_node->msg_node_list == NULL)
      return DCFI_EALLOC;

    msg_node = dst_node->msg_node_list;
    msg_node->m_type = m_type;
    msg_node->dst_node = dst_node;
    msg_node->n_children = 0;
    msg_node->next = NULL;
  }
  for (; msg_node; msg_node = msg_node->next)
  {
    /* msg node already here because we just created it */
    if (msg_node->m_type == m_type)
      return DCFI_OK;
    
    /* end of the list. insert the correct msg_node here */
    if (msg_node->next == NULL)
    {
      msg_node->next = dcfi_msg_node_alloc();
      if (msg_node->next == NULL)
        return DCFI_EALLOC;
      
      msg_node = msg_node->next;
      msg_node->m_type = m_type;
      msg_node->dst_node = dst_node;
      msg_node->n_children = 0;
      msg_node->next = NULL;
      return DCFI_OK;
    }
  }
  /* if this part is reached, something went wrong */
  return DCFI_ERROR;
}

int dcfi_add_msg_child(endpoint_t p_src, endpoint_t p_dst, int p_m_type, endpoint_t c_src, endpoint_t c_dst, int c_m_type)
{
  struct dcfi_msg_node *parent_node = NULL, *child_node = NULL;
  int i;

  parent_node = dcfi_get_msg_node(p_src, p_dst, p_m_type);
  child_node = dcfi_get_msg_node(c_src, c_dst, c_m_type);

  if (parent_node == NULL || child_node == NULL)
    return DCFI_ENOSUCHNODE;

  if (parent_node->n_children >= DCFI_OUTGOING_MAX)
    return DCFI_EOUTGOINGMAX;

  for (i = 0; i < parent_node->n_children; i++)
  {
    if (parent_node->children[i] == child_node)
    {
      return DCFI_OK;
    }
  }

  parent_node->children[parent_node->n_children] = child_node;
  parent_node->n_children++;
  return DCFI_OK;
}

int dcfi_set_default(struct proc *rp, endpoint_t src, endpoint_t dst, int m_type)
{
  /* only set default if no last_msg is set */
  if (priv(rp)->dcfi_last_msg)
    return DCFI_OK;

  /* if specified msg is not yet added, return an error */
  priv(rp)->dcfi_last_msg = dcfi_get_msg_node(src, dst, m_type);
  if (!priv(rp)->dcfi_last_msg)
    return DCFI_ERROR;

  return DCFI_OK;
}

/* called when a server is about to receive a message.
 * sets dcfi_last_msg accordingly. */
void dcfi_recv(endpoint_t src, struct proc *dst, int m_type)
{
  struct priv *dst_p = priv(dst);
  struct dcfi_msg_node *msg_node;
  
  if (dst_p->dcfi_enabled == DCFI_DISABLED)
    return;

  printf("[dcfi] recv %d -%d-> %d\n", src, m_type, dst->p_endpoint);
  msg_node = dcfi_get_msg_node(src, dst->p_endpoint, m_type);
  if (!msg_node)
    msg_node = dcfi_get_msg_node(DCFI_ENDPOINT_ANY, dst->p_endpoint, m_type);

  /* if msg_node was not defined, keep using the last defined received message
   * otherwise, you could essentially ddos a service by sending invalid msgs
   * alternatively, assign the default msg to the node here */
  if (msg_node)
    dst_p->dcfi_last_msg = msg_node;
}

/* called when a server attempts to send a message.
 * decides, based on dcfi_last_msg, whether or not to allow it. */
int dcfi_check (struct proc *caller, endpoint_t dst, int m_type)
{
  struct priv *caller_p = priv(caller);
  struct dcfi_msg_node *last_msg = (struct dcfi_msg_node *)caller_p->dcfi_last_msg;
  struct dcfi_msg_node *child = NULL;
  int i;

  /* DCFI is not configured for this src */
  if (caller_p->dcfi_enabled == DCFI_DISABLED)
  {
    return DCFI_OK;
  }

  /* This should never happen. If no msg has been received yet, a default one is stored here */
  if (last_msg == NULL)
  {
    printf("[dcfi] blocked (!last_msg) %d -%d-> %d\n", caller->p_endpoint, m_type, dst);
    return DCFI_ENOTALLOWED;
  }

  /* Is this message one of the possible child msgs (or one of the whitelisted msgs for src)? */
  for (i = 0; i < last_msg->n_children; i++)
  {
    child = last_msg->children[i];
    if ((child->dst_node->dst == dst || child->dst_node->dst == DCFI_ENDPOINT_ANY) && child->m_type == m_type)
    {
      printf("[dcfi] allowed %d -%d-> %d\n", caller->p_endpoint, m_type, dst);
      return DCFI_OK;
    }
  }

  printf("[dcfi] blocked %d -%d-> %d\n", caller->p_endpoint, m_type, dst);
  return DCFI_ENOTALLOWED;
}
#else
void dcfi_patch_ep(endpoint_t from, endpoint_t to)
{
  return;
}

int dcfi_cleanup(endpoint_t ep)
{
  return DCFI_OK;
}

int dcfi_add_msg_node(endpoint_t src, endpoint_t dst, int m_type)
{
  return DCFI_OK;
}

int dcfi_add_msg_child(endpoint_t p_src, endpoint_t p_dst, int p_m_type, endpoint_t c_src, endpoint_t c_dst, int c_m_type)
{
  return DCFI_OK;
}

int dcfi_set_default(struct proc *rp, endpoint_t src, endpoint_t dst, int m_type)
{
  return DCFI_OK;
}

void dcfi_recv(endpoint_t src, struct proc *dst, int m_type)
{
  return;
}

int dcfi_check (struct proc *caller, endpoint_t dst, int m_type)
{
  return DCFI_OK;
}
#endif

