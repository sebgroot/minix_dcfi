#ifndef _PRIVDCFI_H
#define _PRIVDCFI_H

#define DCFI_ENDPOINT_KERNEL  -1
#define DCFI_ENDPOINT_ANY     -2
#define DCFI_ENDPOINT_SELF    -3

#define DCFI_ENABLED 1
#define DCFI_DISABLED 0

struct priv_dcfi_node {
  endpoint_t src;
  endpoint_t dst;
  int m_type;
};

struct priv_dcfi_child {
  endpoint_t p_src;
  endpoint_t p_dst;
  int p_m_type;
  endpoint_t c_src;
  endpoint_t c_dst;
  int c_m_type;
};

#endif
